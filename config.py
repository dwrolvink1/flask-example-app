class Config(object):
    DEBUG = False
    TESTING = False

class ProductionConfig(Config):
	DEBUG = True
	TESTING = False

class DevelopmentConfig(Config):
	DEVELOPMENT = False
	DEBUG = True
	TESTING = True

class TestingConfig(Config):
	TESTING = True
	TEMPLATES_AUTO_RELOAD = True
