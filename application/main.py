""" App blueprint module for handling basic pages.

Serves the following routes:
index -- the homepage
"""

from flask import Blueprint, render_template, g
from . import title
import datetime

# Initialize Blueprint
main = Blueprint('main', __name__)

# -- Routes
#
# @main.route('/') will bind the render_homepage function to any requests
# to the sites root (e.g. http://localhost/).
#
@main.route('/')
def render_homepage():
	# Set the global title variable
	g.title = 'FBP - Homepage'

	# Get date as a string
	dt = datetime.datetime.now()
	g.date = dt.strftime('%m/%d/%Y')

	# Get the template /templates/index.html and return it.
	# Note that index.html requires base.html, so index.html will be
	# inserted into base.html, and base.html will then be returned. 
	# (See the first line of index.html)
	return render_template('index.html') 
